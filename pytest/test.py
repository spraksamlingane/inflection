import unittest
from inflect import inflect


class MyTestCase(unittest.TestCase):
    def test_non_inflectable(self):
        word = 'flamme'
        inflections = []
        expected = []
        result = inflect(word, inflections)
        self.assertListEqual(expected, result)

    def test_inflection(self):
        word = 'flamme'
        inflections = ['++e', '-', '++er', '++es', '++a']
        expected = ['flamme', None, 'flammer', 'flammes', 'flamma']
        result = inflect(word, inflections)
        self.assertListEqual(expected, result)

    def test_inflection_ea(self):
        word = 'fiskesuppe'
        inflections = ['e/a', 'a', 'or', 'one']
        expected = ['fiskesuppa', 'fiskesuppa', 'fiskesuppor', 'fiskesuppone']
        result = inflect(word, inflections)
        self.assertListEqual(expected, result)

    def test_inflection_st(self):
        word = 'slåss'
        inflections = ['låss/t', 'læss', 'less', 'låss', '-']
        expected = ['slåss', 'slæss', 'sless', 'slåss', None]
        result = inflect(word, inflections)
        self.assertListEqual(expected, result)

    def test_inflection_t(self):
        word = 'forekomast'
        inflections = ['komas|t', 'kjems', 'koms', 'komes', '-']
        expected = ['forekomas', 'forekjems', 'forekoms', 'forekomes', None]
        result = inflect(word, inflections)
        self.assertListEqual(expected, result)


if __name__ == '__main__':
    unittest.main()

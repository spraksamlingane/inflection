import re


def inflect(word, inflection_templates):
    """
    Inflects words according to the templates used by ordbok.uib.no
    :param word: The word to inflect
    :param inflection_templates: A list of inflection templates
    :return: Word forms according to 'inflection_templates'
    """
    if not inflection_templates:
        return []

    if inflection_templates[0] == 'e/a':
        word_mod = re.sub(r'e$', 'a', word)
        inflections_mod = ['a'] + inflection_templates[1:]
    elif (inflection_templates[0] or '').endswith('s/t'):
        word_mod = re.sub(r't$', 's', word)
        inflections_mod = [inflection_templates[0].replace('s/t', 's')] + inflection_templates[1:]
    elif (inflection_templates[0] or '').endswith('|t'):
        word_mod = word[:-1]
        inflections_mod = [inflection_templates[0][:-2]] + inflection_templates[1:]
    else:
        word_mod = word
        inflections_mod = inflection_templates

    u = next(x or '' for x in inflections_mod if x != '-')
    stem_len = len(word_mod) - len(u)
    stem = word_mod[:stem_len]
    tail = word_mod[stem_len:]
    tra = dict(pair for pair in zip(u, tail))
    return [stem + ''.join(list(map(lambda x: tra.get(x, x), form))) if form and form != '-' else (stem if not form else None) for form in inflections_mod]
